package ar.edu.unlu.observer;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class ObservableProxy implements Observable {
	private PropertyChangeSupport changes = new PropertyChangeSupport(this);
	
	@Override
    public void removePropertyChangeListener(PropertyChangeListener l) {
        changes.removePropertyChangeListener(l);
    }
    
	@Override
	public void addPropertyChangeListener(String propertyName, PropertyChangeListener l) {
		if(propertyName != null)
			changes.addPropertyChangeListener(propertyName,l);
		else
			changes.addPropertyChangeListener(l);
		
	}

	@Override
	public void notifyListeners(String propertyName, Object oldValue, Object newValue) {
		changes.firePropertyChange(propertyName, oldValue, newValue);		
	}

}
