package ar.edu.unlu.observer;

public interface Notifier {
	/**
	 * Este método debería notificar a los observadores.
	 * @param source
	 * @param propertyName
	 * @param oldValue
	 * @param newValue
	 */
	void notifyListeners(String propertyName, Object oldValue, Object newValue);
}
