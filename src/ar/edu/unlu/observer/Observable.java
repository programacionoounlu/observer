package ar.edu.unlu.observer;

import java.beans.PropertyChangeListener;

/**
 * Interfaz que debería implementar la clase observada 
 * usando un atributo de tipo PropertyChangeSupport para 
 * manejar las notificaciones a los observadores.
 * 
 * @author unlu
 *
 */
public interface Observable extends Notifier{
	/**
	 * Agrega un objeto de tipo PropertyChangeListener como observador de 
	 * la propiedad 'propertyName'. En caso de que propertyName sea null, se 
	 * agrega como observador de todas las propiedades.
	 * @param propertyName
	 * @param l
	 */
	void addPropertyChangeListener(String propertyName, PropertyChangeListener l);
	
	/**
	 * Elimina el observador
	 * @param l
	 */
	void removePropertyChangeListener(PropertyChangeListener l);
	
}
