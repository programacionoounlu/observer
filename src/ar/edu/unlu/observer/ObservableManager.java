package ar.edu.unlu.observer;

import java.beans.PropertyChangeListener;
import java.util.HashMap;

/**
 * Clase que administra Observables. Implementa el patron singleton ya que debe 
 * ser el único administrador disponible en el sistema. 
 * 
 * Una clase observada se puede agregar como tal usando el método addObservable. Si
 * ya se posee un objeto que implementa la interfaz Observable se puede pasar como parámetro,
 * sino se creará un ObservableProxy que realiza una implementacion básica de Observable.
 * 
 * Los observadores pueden agregarse como tales obteniendo el observable asociado con getObservable.
 * De esta misma manera las clases observadas podran notificar a sus observadores.
 * 
 * Tiene un problema grave de seguridad: desde cualquier lado podría obtener el notificador de alguna 
 * clase y notificar cosas.
 * @author unlu
 *
 */
public class ObservableManager {
	private static ObservableManager instance = new ObservableManager();
	
	private HashMap<String, Observable> observados;
	
	private ObservableManager() {
		observados = new HashMap<String, Observable>();
	}
	
	public static ObservableManager getInstance() {
		return instance;
	}
	
	/**
	 * Crea un ObservableProxy para que resuelva toda la responsabilidad
	 * de notificaciones y llama al metodo addObservable aplicando el patron
	 * adapter. Devuelve un Notifier para que la clase observada pueda notificar.
	 * @param claseObservada
	 * @throws ObservableAlreadyExists 
	 */
	public Notifier addObservable(String claseObservada) throws ObservableAlreadyExists {
		ObservableProxy proxy = new ObservableProxy();
		return this.addObservable(claseObservada, proxy);
		
	}
	
	/**
	 * Agrega la claseObservada a la lista de clases observadas junto con su observable.
	 * Si la clase observada ya se encuentra registrada entonces lanza la excepcion 
	 * ObservableAlreadyExists.
	 * 
	 * @param claseObservada
	 * @param observable
	 * @return Una instancia de una clase que implementa Observable
	 * @throws ObservableAlreadyExists 
	 */
	public Notifier addObservable(String claseObservada, Observable observable) throws ObservableAlreadyExists {
		if(observados.get(claseObservada) == null) {
			this.observados.put(claseObservada, observable);
			return observable;
		}else
			throw new ObservableAlreadyExists("La claseObservada ya se encuentra registrada");
	}
	
	/**
	 * Agrega un PropertyChangeListener al atributo de la clase observable.
	 * @param claseObservable
	 * @return
	 */
	public void addPropertyChangeListener(String claseObservable, String property, PropertyChangeListener listener) {
		this.observados.get(claseObservable).addPropertyChangeListener(property, listener);;
	}
	
	/**
	 * Elimina un PropertyChangeListener de la clase observable.
	 * @param claseObservable
	 * @return
	 */
	public void removePropertyChangeListener(String claseObservable, PropertyChangeListener listener) {
		this.observados.get(claseObservable).removePropertyChangeListener(listener);;
	}
}
