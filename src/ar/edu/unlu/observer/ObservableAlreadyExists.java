package ar.edu.unlu.observer;

public class ObservableAlreadyExists extends Exception {

	public ObservableAlreadyExists(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 622236902720353719L;

}
