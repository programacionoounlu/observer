package ar.edu.unlu.ejemplosClasesObservables;

import ar.edu.unlu.observer.Notifier;
import ar.edu.unlu.observer.ObservableAlreadyExists;
import ar.edu.unlu.observer.ObservableManager;

/**
 * Esta clase usa ObservableManager para notificar sus eventos
 * @author unlu
 *
 */
public class ClaseConPropiedadesManejadas {
	private Notifier notificador;
	public static final String observableId = "ClaseConPropiedadesManejadas";
	private int count;
	
	public final int E_SUMANDO = 0;
	public final int E_NOT_SUMANDO = 1;
	private int estado;
	
	public ClaseConPropiedadesManejadas() {
		estado = E_SUMANDO;
		count = 0;
		ObservableManager obsManager = ObservableManager.getInstance();
		try {
			notificador = obsManager.addObservable(observableId);
		} catch (ObservableAlreadyExists e) {
			e.printStackTrace();
		}
	}
	
	public void sumar() {
		if (estado == E_SUMANDO) {
			count += 1;
			notificador.notifyListeners("contador", null, count);
		}
	}
	
	public void stopCounting() {
		this.estado = E_NOT_SUMANDO;
		notificador.notifyListeners("estado", null, estado);
	}
}
