package ar.edu.unlu.ejemplosClasesObservables;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import ar.edu.unlu.observer.Observable;

public class ClaseConPropiedades implements Observable{
	private PropertyChangeSupport changes = new PropertyChangeSupport(this);
	
	private int count;
	
	public final int E_SUMANDO = 0;
	public final int E_NOT_SUMANDO = 1;
	private int estado;
	
	public ClaseConPropiedades() {
		estado = E_SUMANDO;
		count = 0;
		
	}
	
	public void sumar() {
		if (estado == E_SUMANDO) {
			count += 1;
			notifyListeners("contador", null, count);
		}
	}
	
	public void stopCounting() {
		this.estado = E_NOT_SUMANDO;
		notifyListeners("estado", null, estado);
	}

	@Override
    public void removePropertyChangeListener(PropertyChangeListener l) {
        changes.removePropertyChangeListener(l);
    }
    
	@Override
	public void addPropertyChangeListener(String propertyName, PropertyChangeListener l) {
		if(propertyName != null)
			changes.addPropertyChangeListener(propertyName,l);
		else
			changes.addPropertyChangeListener(l);
		
	}

	@Override
	public void notifyListeners(String propertyName, Object oldValue, Object newValue) {
		changes.firePropertyChange(propertyName, oldValue, newValue);		
	}
}
