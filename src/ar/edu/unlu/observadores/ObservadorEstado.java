package ar.edu.unlu.observadores;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class ObservadorEstado implements PropertyChangeListener{

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		System.out.println("----\tObservador Estado\t----");
		System.out.println("ESTADO CAMBIADO: "+evt.getNewValue());
		System.out.println("------------------------------");
	}

}
