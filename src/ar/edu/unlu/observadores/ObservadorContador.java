package ar.edu.unlu.observadores;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class ObservadorContador implements PropertyChangeListener{

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		System.out.println("----\tObservador Contador\t----");
		System.out.println("El valor del contador ahora es: "+evt.getNewValue());
		System.out.println("------------------------------");
	}

}
