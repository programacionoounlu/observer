package ar.edu.unlu.main;

import ar.edu.unlu.ejemplosClasesObservables.ClaseConPropiedades;
import ar.edu.unlu.observadores.ObservadorContador;
import ar.edu.unlu.observadores.ObservadorEstado;

public class MainImplementacionPropia {

	public static void main(String[] args) {
		/*
		 * Ejemplo agregando los observadores a una propiedad especifica.
		 */
		System.out.println("Prueba 1------------------");
		ClaseConPropiedades ccp = new ClaseConPropiedades();
		ObservadorContador oc = new ObservadorContador();
		ObservadorEstado oe = new ObservadorEstado();
		
		ccp.addPropertyChangeListener("estado", oe);
		ccp.addPropertyChangeListener("contador", oc);
		
		ccp.sumar();
		ccp.sumar();
		ccp.sumar();
		
		ccp.stopCounting();
		
		System.out.println("Prueba 2------------------");
		/*
		 * Creo otra instancia de clase con propiedades y agrego los observadores
		 * a todos los atributos. No se van a entender los mensajes ahora porque los
		 * dos observadores van a ser notificados para ambas propiedades.
		 */
		ClaseConPropiedades ccp2 = new ClaseConPropiedades();
		ccp2.addPropertyChangeListener(null, oe);
		ccp2.addPropertyChangeListener(null, oc);
		
		ccp2.sumar();
		ccp2.sumar();
		ccp2.sumar();
		
		ccp2.stopCounting();
	}

}
