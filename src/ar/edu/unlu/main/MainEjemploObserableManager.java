package ar.edu.unlu.main;

import ar.edu.unlu.ejemplosClasesObservables.ClaseConPropiedadesManejadas;
import ar.edu.unlu.observadores.ObservadorContador;
import ar.edu.unlu.observadores.ObservadorEstado;
import ar.edu.unlu.observer.ObservableManager;

public class MainEjemploObserableManager {

	public static void main(String[] args) {
		System.out.println("Ejemplo 1 ------------------------------------------------------");
		ClaseConPropiedadesManejadas ccpm = new ClaseConPropiedadesManejadas();
		ObservadorContador oc = new ObservadorContador();
		ObservadorEstado oe = new ObservadorEstado();
		
		ObservableManager.getInstance().addPropertyChangeListener(ClaseConPropiedadesManejadas.observableId, 
																	"estado", oe);
		ObservableManager.getInstance().addPropertyChangeListener(ClaseConPropiedadesManejadas.observableId, 
																	"contador", oc);
		ccpm.sumar();
		ccpm.sumar();
		ccpm.sumar();
		
		ccpm.stopCounting();
	}
	
}
