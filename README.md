# Ejemplo de observer

En este proyecto se impementó un ejemplo de observer utilizando las clases PropertyChangeSupport y PropertyChangeListener del paquete [java.beans](https://docs.oracle.com/javase/8/docs/api/java/beans/package-summary.html)

Dentro del paquete observer se encuentra la interface Observable, la cual deben implementar correctamente los objetos observados para poder agregar y notificar a los observadores (PropertyChangeListener). La clase ar.edu.unlu.ejemplosClasesObservables.ClaseConPropiedades es un ejemplo de implementacion de esa interface.

Tambien se encuentra disponible la clase ObservableManager. Esta clase es un singleton que administra observables de todo el sistema. La idea es que cada clase le pida al manager que la agregue como Observable y los observadores le pidan directamente al manager que lo agregue como observador de alguna clase. 
Al agregarse como Observable se creará una instancia de ObservableProxy que tiene la responsabilidad de administrar observadores y de notificar. Esta instancia será devuelta a la clase observada como Notifier. 
Tambien se puede crear una instancia de Observable y pasarla directamente como parámetro, este sería el caso en que quisieramos una implementacion diferente a ObservableProxy.

# Como ejecutar demostraciones 

En el paquete ar.edu.unlu.main existen dos clases que son puntos de entradas para las diferentes formas de usar observer mencionadas anteriormente.